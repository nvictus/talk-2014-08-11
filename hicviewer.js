//
// File Select handler:
//  - Load in a Hi-C heatmap matrix in binary .npy format
//  - Paint the scaled intensities onto a canvas context
//  - Embed the base64-encoded dataURL into a img tag
//  - Apply pan/zoom functionality using jquery panzoom plugin
//
function handleFileSelect(evt) {
  var file = evt.target.files[0];
  var reader = new FileReader();
  reader.onload = function() {
    // create a canvas
    var canvas = document.createElement("canvas");
    var ctx = canvas.getContext("2d");
    
    // prevent anti-aliasing (still a bug in chrome)
    ctx.imageSmoothingEnabled = false;
    ctx.webkitImageSmoothingEnabled = false;
    ctx.mozImageSmoothingEnabled = false;
    
    // use d3 to make a logarithmic color scale
    var color = d3.scale.log()
        .domain([1, 2000.0])
        .range(["white", "red"]);

    // read file contents
    var buf = reader.result;
    var array = parseNumpyFileBuffer(buf);
    var matrix = array.data,
        lenX = array.shape[0],
        lenY = array.shape[1];

    // paint the matrix
    canvas.width = lenX;
    canvas.height = lenY;
    imgData = ctx.createImageData(lenX, lenY);
    for (var i = 0, p = -1; i < lenY; ++i) {
      for (var j = 0; j < lenX; ++j) {

        var value = matrix[lenY*i + j];
        
        var c = d3.rgb(value < 1.0 ? "white" : color(value));

        imgData.data[++p] = c.r;
        imgData.data[++p] = c.g;
        imgData.data[++p] = c.b;
        imgData.data[++p] = 255;
      }
    }
    ctx.putImageData(imgData, 0, 0);

    // encode the canvas contents and apply to img tag's src
    var heatmap = $('.heatmap');
    heatmap.attr("src", canvas.toDataURL("image/png"));
    
    // resize the bounding window
    // TODO: work on this!
    var side = heatmap.attr("width");
    var offset = side/Math.sqrt(2) - side/2.0;
    var hicWindow = $('.hic-window');
    hicWindow.css("width", side*Math.sqrt(2));
    hicWindow.css("height", side/Math.sqrt(2));
    hicWindow.css("visibility", "visible");

    // apply panzoom
    (function() {
      var viewer = $('#hic-viewer');
      var container = viewer.find('.panzoom-container');
      container.panzoom({
        $zoomIn: viewer.find(".zoom-in"),
        $zoomOut: viewer.find(".zoom-out"),
        $zoomRange: viewer.find(".zoom-range"),
        $reset: viewer.find(".reset"),
        // more options here
        maxScale: 10,
        startTransform: 'translate(145px, -350px)'
      });
    })();
  };
  reader.readAsArrayBuffer(file);
};
document.getElementById('fileloader')
    .addEventListener('change', handleFileSelect, false);


//  
// Client-side parser for .npy files
//
function bytes2ascii(buf) {
  return String.fromCharCode.apply(null, new Uint8Array(buf));
}

function readUint16LE(buffer) {
  view = new DataView(buffer);
  var val = view.getUint8(0);
  val |= view.getUint8(1) << 8;
  return val;
}

function parseNumpyFileBuffer(buf) {
  var magic = bytes2ascii(buf.slice(0,6));
  if (magic.slice(1,6) != 'NUMPY') {
    throw new Error('unknown file type');
  }

  var version = new Uint8Array(buf.slice(6,8)),
      headerLength = readUint16LE(buf.slice(8,10)),
      s = bytes2ascii(buf.slice(10, 10+headerLength)),
      data = buf.slice(10+headerLength);

  eval("var info = " + s.toLowerCase().replace('(','[').replace('),',']'));

  if (info.descr === "|u1") {
    data = new Uint8Array(data);
  } else if (info.descr === "|i1") {
    data = new Int8Array(data);
  } else if (info.descr === "<u2") {
    data = new Uint16Array(data);
  } else if (info.descr === "<i2") {
    data = new Int16Array(data);
  } else if (info.descr === "<u4") {
    data = new Uint32Array(data);
  } else if (info.descr === "<i4") {
    data = new Int32Array(data);
  } else if (info.descr === "<f4") {
    data = new Float32Array(data);
  } else if (info.descr === "<f8") {
    data = new Float64Array(data);
  } else {
    throw new Error('unknown numeric dtype')
  }

  return {
    shape: info.shape,
    fortran_order: info.fortran_order,
    data: data
  };
}

function loadNumpyArray(url, onload) {
  var xhr = new XMLHttpRequest();
  xhr.onload = function(e) {
    var buf = xhr.response; // not responseText
    var ndarray = parseNumpyFileBuffer(buf);
    onload(ndarray);
  };
  xhr.open("GET", url, true);
  xhr.responseType = "arraybuffer";
  xhr.send(null);
}

// }